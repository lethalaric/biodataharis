package com.emerio.biodata.addressservice.controller;

import com.emerio.biodata.addressservice.repository.AddressRepository;
import com.emerio.biodata.addressservice.entity.Address;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;


@RestController
public class AddressController {
    @Autowired private AddressRepository addressRepository;

    public AddressController(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    @RequestMapping(value="/addresss/", method=RequestMethod.GET)
    public ResponseEntity<Iterable<Address>> getAll() {
        return new ResponseEntity<Iterable<Address>> ((Iterable<Address>) this.addressRepository.findAll(), HttpStatus.OK);
    }
}