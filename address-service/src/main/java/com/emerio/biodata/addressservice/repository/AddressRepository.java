package com.emerio.biodata.addressservice.repository;

import org.springframework.data.repository.CrudRepository;
import com.emerio.biodata.addressservice.entity.Address;


public interface AddressRepository extends CrudRepository<Address, Long> {

}