package com.emerio.biodata.addressservice.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Address {

    @Id
    @GeneratedValue
    private int id;

    private String provinsi;
    private String kota;
    private String kecamatan;
    private String kodePos;
    private String jalan;

    public void setProvinsi(String value) {
        this.provinsi = value;
    }

    public void setKota(String value) {
        this.kota= value;
    }

    public void setKecamatan(String value) {
        this.kecamatan = value;
    }

    public void setKodePos(String value) {
        this.kodePos= value;
    }

    public void setJalan(String value) {
        this.jalan = value;
    }

    public int getId() {
        return this.id;
    }

    public String getProvinsi() {
        return this.provinsi;
    }

    public String getKota() {
        return this.kota;
    }

    public String getKecamatan() {
        return this.kecamatan;
    }

    public String getKodePos() {
        return this.kodePos;
    }

    public String getJalan() {
        return this.jalan;
    }
} 