package com.emerio.biodata.biodataservice;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Biodata {
	@Id
	@GeneratedValue
	private Long id;
	private String name;
    private String dateofBirth;
    private String address;
    private String job;
	
	public Biodata() {
		super();
	}

	public Biodata(Long id, String name, String dateofBirth, String address, String job) {
		super();
		this.id = id;
		this.name = name;
        this.dateofBirth = dateofBirth;
        this.address = address;
        this.job = job;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDateofBirth() {
		return dateofBirth;
	}
	public void setDateofBirth(String dateofBirth) {
		this.dateofBirth = dateofBirth;
    }
    public String getAddress(){
        return address;
    }
    public void setAddress(){
        this.address=address;
    }
    public String getJob(){
        return job;
    }
    public void setJob(){
        this.job=job;
    }
		
}