package com.emerio.biodata.biodataservice;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class BiodataResource {

	@Autowired
	private BiodataRepository biodataRepository;

	@GetMapping("/biodata")
	public List<Biodata> retrieveAllBiodata() {
		return biodataRepository.findAll();
	}

	@GetMapping("/biodata/{id}")
	public Biodata retrieveBiodata(@PathVariable long id) {
		Optional<Biodata> biodata = biodataRepository.findById(id);

		if (!biodata.isPresent())
			throw new BiodataNotFoundException("id-" + id);

		return biodata.get();
	}

	@DeleteMapping("/biodata/{id}")
	public void deleteBiodata(@PathVariable long id) {
		biodataRepository.deleteById(id);
	}

	@PostMapping("/biodata")
	public ResponseEntity<Object> createBiodata(@RequestBody Biodata biodata) {
		Biodata savedBiodata = biodataRepository.save(biodata);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedBiodata.getId()).toUri();

		return ResponseEntity.created(location).build();

	}
	
	@PutMapping("/biodata/{id}")
	public ResponseEntity<Object> updateBiodata(@RequestBody Biodata biodata, @PathVariable long id) {

		Optional<Biodata> biodataOptional = biodataRepository.findById(id);

		if (!biodataOptional.isPresent())
			return ResponseEntity.notFound().build();

		biodata.setId(id);
		
		biodataRepository.save(biodata);

		return ResponseEntity.noContent().build();
	}
}