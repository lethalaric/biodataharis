package com.emerio.biodata.biodataservice;

public class BiodataNotFoundException extends RuntimeException {

	public BiodataNotFoundException(String exception) {
		super(exception);
    }
}
